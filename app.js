const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

keywords=[
    "EVELYN",
    "GEORGIE",
    "CONRAD",
    "HENRY",
    "CAROLA",
    "ANABELLE",
    "PENELOPE",
    "CAROLINA",
    "CHRISTOPHER",
    "ELLIOT",
    "AUSTIN",
    "HELOISE",
    "ELLEEN",
    "LUCAS",
    "GREYSON",
    "HANNAH",
    "VIVIENNE",
    "HARRIET",
    "JACKSON",
    "SAMEER",
    "EAVES",
    "PALOMA",
    "LILA",
    "ROMEO",
    "NICOLO",
    "KHALAN",
    "ALEX",
    "EVEN",
    "MS. HIME",
    "LINCOLN",
    "STRIKE",
    "ART",
    "P.E.",
    "MUSIC",
    "LIBRARY",
    "MATH",
    "HISTORY",
    "CHICAGO",
    "ILLINOIS",
    "SCHOOL",
    "SPRINGFIELD",
    "HOMEWORK",
    "STARS",
    "VACATION",
    "SUMMER",
    "KEMPER",
    "CPS",
    "CAMP",
    "GAMES",
    "VIRTUAL",
    "CLASS"
];


app.get('/', function(req, res) {
    res.render('index.ejs');
});

app.get('/controller', function(req, res) {
    res.render('controller.ejs');
});

app.get('/mandala', function(req, res) {
    res.render('mandala.ejs');
});


setInterval(tenSecondFunction, 10000);

function tenSecondFunction() {
    try {
io.emit('keywords', JSON.stringify(keywords));
    }
    catch(err) {
console.log(err);
    }
}

var tkeywords=[]
function reset() {
    tkeywords=[];
    for (i=0 ; i < keywords.length ; i++) {
tkeywords.push(keywords[i]);
    }
    console.log(tkeywords);
}

function getkeyword() {
    var randomItem = tkeywords[Math.floor(Math.random()*tkeywords.length)];
    var tarr=[];
    for (i=0 ; i < tkeywords.length ; i++) {
if (tkeywords[i] != randomItem) {
    tarr.push(tkeywords[i]);
}
    }
    tkeywords=tarr;
    return randomItem;
}

reset();
io.sockets.on('connection', function(socket) {
    socket.on('sendreset', function() {
console.log("received sendreset");
reset();
        io.emit('reset', '{}');
    });

    socket.on('sendkeyword', function() {
console.log("received sendkeyword");
        io.emit('keyword', getkeyword());
    });

    socket.on('*',function(event, data) {
console.log('Event received: ' + event + ' - data:' + JSON.stringify(data));
    });
    
});

const server = http.listen(3000, function() {
    console.log('listening on *:3000');
});
